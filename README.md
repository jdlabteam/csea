# CSEA

## Description
This repository contains R scripts for making CSEA plots locally. These scripts produce the same plots produced by the CSEA tool on the JDlab website (as of 4/1/18). 

Note: the code that the website runs on is in a different format and should ultimately be moved into this github repository.

## Repository files
* code/
	* Example 1 of making a plot from AutDB list.R
	* Example 2 of making a plot from AutDB list.R
	* bullseye_functions.R - functions to make CSEA plots based on TRAP data
	* bullseye_functions_for_Zeisel2015.R -  functions to make CSEA plots based on expression data from Zeisel et al. 2015 (ultimately, this should be merged with the above script. the only difference between these two scripts is that some of the plotting parameters are changed)
	* extract_genes.R - utility function to output candidate genes overlapping with cell-type markers at different pSI thresholds
	* run_csea.R - functions that wrap the fisher test and CSEA plotting code 
	* get_pSI_Zeisel2015.Rmd - this is the code Allie used to process the Zeisel 2015 expression set and generate pSI from it.
	* load_packages.R - script to install necessary packages
	* set_coords.R - example script for loading in an expression dataset and modifying the plotting coordinates (currently done manually, by dragging and clicking points)
* objs/
	* pSI_coords.Rdata - plotting objects to be used for TRAP-based CSEA plots
	* pSI_coors_Zeisel2015.rdata - pltting objects to be used for CSEA plots based on Zeisel data
	* RPKM_Zeisel2015.Rds - expression data from Zeisel 2015 paper generated in get_pSI_Zeisel2015.Rmd, used to generate CSEA tree in set_coords.R
	
## Instructions
Run the two Example scripts

