setwd("~/Desktop/CSEA/") # change to your working directory
source("code/load_packages.R")
source('code/bullseye_functions_for_Zeisel2015.R')  #functions for plotting CSEA
source("code/run_csea.R")
library(pSI)
library(pSI.data)
data(mouse) #this is part of CSEA paper/ pSI.data package.  

p_vals <- run_csea_wrapper(gene_list = candidate.genes$AutDB, 
                           adjust = TRUE,
                           bkgrd_mh = FALSE,
                           path = "code/bullseye_functions.R",
                           objs_path = "objs/pSI_coords.rdata",
                           psi_path = "objs/pSI_TRAP.Rds", output_overlap = T,
                           plot_path = "CSEA_AutDB_TRAP.pdf")


p_vals <- run_csea_wrapper(gene_list = candidate.genes$AutDB, 
                           adjust = TRUE,
                           bkgrd_mh = FALSE,
                           path = "code/bullseye_functions_for_Zeisel2015.R",
                           objs_path = "objs/pSI_coords_Zeisel2015.rdata",
                           psi_path = "objs/pSI_Zeisel2015.Rds", output_overlap = T,
                           plot_path = "CSEA_AutDB_Zeisel2015.pdf")
